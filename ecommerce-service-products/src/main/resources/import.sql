INSERT INTO product (name,   price, weight) VALUES('Camera Panasonic', 800, 1);
INSERT INTO product (name,   price, weight) VALUES('Speaker Sony', 700, 5);
INSERT INTO product (name,   price, weight) VALUES('Iphone X', 1000, 1);
INSERT INTO product (name,   price, weight) VALUES('Sony Notebook', 1000, 2);
INSERT INTO product (name,   price, weight) VALUES('Hewlett Packard', 500, 1);
INSERT INTO product (name,   price, weight) VALUES('Bianchi', 600, 5);
INSERT INTO product (name,   price, weight) VALUES('Nike', 100, 1);
INSERT INTO product (name,   price, weight) VALUES('Adidas', 200, 10);
INSERT INTO product (name,   price, weight) VALUES('Reebok', 300, 1);

INSERT INTO category (name) VALUES('Cameras');
INSERT INTO category (name) VALUES('Speakers');
INSERT INTO category (name) VALUES('Cell Phones');
INSERT INTO category (name) VALUES('Shoes');
INSERT INTO category (name) VALUES('Laptos');