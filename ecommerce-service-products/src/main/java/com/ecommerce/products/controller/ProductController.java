/**
 * 
 */
package com.ecommerce.products.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.common.models.entity.Product;
import com.ecommerce.products.service.ProductService;

/**
 * <Descripcion de la clase>
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/") 
public class ProductController {
	
	private  final Logger log = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductService productService;

	
	@GetMapping("/products")
	public List<Product> findAll() {
		log.info("REST request to get All  Categories");
		return productService.findAll();
	}

	@ResponseStatus(HttpStatus.OK)
	@PostMapping("/products")
	public Product save(@RequestBody Product category) {
		return productService.save(category);
	}
	
	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/products")
	public Product update(@RequestBody Product category) {
		return productService.save(category);
	}


	@GetMapping("/products/{id}")
	public ResponseEntity<Product> findById(@PathVariable Integer id) {
		Optional<Product> product =productService.findById(id);
		return product.map(response -> ResponseEntity.ok().body(response))
	            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
		
	}
	
	
	@DeleteMapping("/products/{id}")
	public void delete(@PathVariable Integer id) {
		productService.delete(id);
	}
	
}
