/**
 * 
 */
package com.ecommerce.products.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.common.models.entity.Product;
import com.ecommerce.products.model.repository.ProductRepository;
import com.ecommerce.products.service.ProductService;

/**
 * <Descripcion de la clase>
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
@Service
public class ProductServiceImpl implements ProductService{
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Product> findAll() {
		return productRepository.findAll();
	}

	@Override
	@Transactional
	public Product save(Product product) {
		return productRepository.save(product);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Product> findById(Integer id) {
		return productRepository.findById(id);
	}
	
	@Override
	@Transactional
	public void delete(Integer  id) {
		productRepository.deleteById(id);
		
	}
	
}
