/**
 * 
 */
package com.ecommerce.products.service;

import java.util.List;
import java.util.Optional;

import com.ecommerce.common.models.entity.Product;

/**
 * <Descripcion de la clase>
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
public interface ProductService {
	
	public List<Product> findAll() ;

	public Product save(Product product);

	public Optional<Product> findById(Integer id) ;
	
	public void delete(Integer id);
	
}
