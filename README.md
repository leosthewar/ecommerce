﻿# ecommerce

 Aplicacion catalogo de productos, utilizando microservicios

## Herramientas, tecnologías y lenguajes

- Sistema operativo:  Linux Mint 19
- Java 8
- Angular 5
- Spring boot 2.1.4
- Eclipse IDE
- Visual Studio
- Docker
- Docker compose
- Microservicios
- base de datos MySQL 5.7
- Elastic ELK stack 7.2.0 (monitoreo)

## Diagrama

![Screenshot](diagrama-ecommerce.png)

## Instrucciones de ejecución:

## Con docker-compose

- En el directorio raíz del repositorio ejecutar el comando: `mvn clean install`
- Para el ELK, En el directorio raíz del repositorio ejecutar
	- para beats `sudo chown root -R filebeat/` y `sudo chmod 600 -R filebeat/ `   ( https://github.com/primait/docker-filebeat/issues/13 )
	- para elasticsearch  ( https://www.elastic.co/guide/en/elasticsearch/reference/6.2/docker.html#_notes_for_production_use_and_defaults )  

	
		`mkdir es_datadir`  

		`chmod g+rwx es_datadir`  

	
		`chgrp 1000 es_datadir` 


- En el directorio raíz del repositorio ejecutar el comando: `docker-compose up --build`

	- para correr sin hacer build ( generar las imagenes docker de los servicios ecommerce )  ` docker-compose up`
	- para correr solo base de datos `docker-compose -f docker-compose-db.yml up `
	- para todos los servicios sin el ELK stack `docker-compose -f docker-compose-sin-elk.yml up `

#### Una ves se ejecutados los comandos deseados

- Los microservicios corren en localhost en  los siguientes puertos
	- ecommerce-registry : 8761 (servidor eureka)
	- ecommerce-api-gateway : 9090 (servicio gateway)
	- ecommerce-store : aleatorio (servicio store)
- Los servicios del ELK corren en localhost en los siguientes puertos 

	-  kibana:5601
	-  elasticsearch:9200,9300
	-  logstach:25826,5044
- El proyecto ecommerce-store-app (front) corre en localhost en el puerto 8080
  
## Instrucciones de ejecución manuales
#### Backend

Para correr los proyectos spring, se debe entrar al directorio del proyecto deseado y ejecutar  ` mvn spring-boot:run`

#### Frontend
 
- En el directorio ecommerce-store-app ejecutar el comando `npm install` para descargar las dependencias
- Una vez descargadas las dependencias ejecutar el comando `ng serve` para correr la aplicación
- El proyecto se ejecuta en localhost en el puerto 4200
