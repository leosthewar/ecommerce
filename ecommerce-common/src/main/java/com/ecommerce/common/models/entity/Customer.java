/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.common.models.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * <Descripcion de la clase>
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcustomer")
    private Integer idcustomer;
    @Column(name = "name")
    private String name;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "identification")
    private Long identification;
    @Column(name = "email")
    private Long email;
    @Column(name = "phone")
    private Long phone;

    public Customer() {
    }

    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idcustomer == null) ? 0 : idcustomer.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (idcustomer == null) {
			if (other.idcustomer != null)
				return false;
		} else if (!idcustomer.equals(other.idcustomer))
			return false;
		return true;
	}


	/**
	 * @return the idcustomer
	 */
	public Integer getIdcustomer() {
		return idcustomer;
	}

	/**
	 * @param idcustomer the idcustomer to set
	 */
	public void setIdcustomer(Integer idcustomer) {
		this.idcustomer = idcustomer;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the identification
	 */
	public Long getIdentification() {
		return identification;
	}

	/**
	 * @param identification the identification to set
	 */
	public void setIdentification(Long identification) {
		this.identification = identification;
	}

	/**
	 * @return the email
	 */
	public Long getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(Long email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public Long getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(Long phone) {
		this.phone = phone;
	}


}
