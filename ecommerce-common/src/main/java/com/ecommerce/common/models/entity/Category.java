/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.common.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;



@Entity
@Table(name = "category")
public class Category implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "idcategory")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idcategory;
	@Column(name = "name")
	private String name;
	@Lob
	@Column(name = "image")
	private byte[] image;
	
	@OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
	private List<Subcategory> subcategoryList;

	public Category() {
	}

	public Category(Integer idcategory) {
		this.idcategory = idcategory;
	}

	public Integer getIdcategory() {
		return idcategory;
	}

	public void setIdcategory(Integer idcategory) {
		this.idcategory = idcategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	@XmlTransient
	public List<Subcategory> getSubcategoryList() {
		return subcategoryList;
	}

	public void setSubcategoryList(List<Subcategory> subcategoryList) {
		this.subcategoryList = subcategoryList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idcategory != null ? idcategory.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Category)) {
			return false;
		}
		Category other = (Category) object;
		if ((this.idcategory == null && other.idcategory != null)
				|| (this.idcategory != null && !this.idcategory.equals(other.idcategory))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "store.Category[ idcategory=" + idcategory + " ]";
	}

}
