import { Component, OnInit } from '@angular/core';
import { Product } from './product';
import { ProductService } from './product.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html'
})
export class ProductsComponent implements OnInit {

  products: Product[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.productService.getProducts().subscribe(
      products => this.products = products
    );
  }

  delete(product: Product): void {
    swal({
      title: 'Are you sure?',
      text: `Are you sure you want to delete the product ${product.name} ?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.productService.delete(product.idproduct).subscribe(
          response => {
            this.products = this.products.filter(cli => cli !== product)
            swal(
              'Product deleted!',
              `Product ${product.name} deleted succesfully.`,
              'success'
            )
          }
        )

      }
    })
  }

}
