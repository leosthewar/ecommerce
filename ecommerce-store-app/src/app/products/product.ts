export class Product {
  idproduct: number;
  name: string;
  description: string;
  weight: number;
  price: number;
  subcategory?: number;
}
