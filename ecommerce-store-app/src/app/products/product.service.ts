import { Injectable } from '@angular/core';
import { Product } from './product';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ProductService {
  private urlEndPoint: string = 'http://localhost:9090/api/store/sp/products';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http: HttpClient) { }

  getProducts(): Observable<Product[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response as Product[])
    );
  }

  create(product: Product) : Observable<Product> {
    product.idproduct =null;
    return this.http.post<Product>(this.urlEndPoint, product, {headers: this.httpHeaders})
  }

  getProduct(idproduct): Observable<Product>{
    return this.http.get<Product>(`${this.urlEndPoint}/${idproduct}`)
  }

  update(product: Product): Observable<Product>{
    return this.http.put<Product>(`${this.urlEndPoint}`, product, {headers: this.httpHeaders})
  }

  delete(idproduct: number): Observable<Product>{
    return this.http.delete<Product>(`${this.urlEndPoint}/${idproduct}`, {headers: this.httpHeaders})
  }

}
