import { Component, OnInit } from '@angular/core';
import {Product} from './product'
import {ProductService} from './product.service'
import {Router, ActivatedRoute} from '@angular/router'
import swal from 'sweetalert2'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class ProductFormComponent implements OnInit {

  private product: Product = new Product()
  private titulo:string = "Create Product"

  constructor(private productService: ProductService,
  private router: Router,
  private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  
    this.loadProduct()
  }

  loadProduct(): void{
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id){
        this.titulo="Edit product";
        this.productService.getProduct(id).subscribe( (product) => this.product = product)
      }
    })
  }

  create(): void {
    this.productService.create(this.product)
      .subscribe(product => {
        this.router.navigate(['/products'])
        swal('Product', `Product ${product.name} created succesfully!`, 'success')
      }
      );
  }

  update():void{
    this.productService.update(this.product)
    .subscribe( product => {
      this.router.navigate(['/products'])
      swal('Product ', `Product ${product.name} updated succesfully!`, 'success')
    }

    )
  }

}
