import { Component, OnInit } from '@angular/core';
import { Category } from './category';
import { CategoryService } from './category.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html'
})
export class CategoriesComponent implements OnInit {

  categories: Category[];

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.categoryService.getCategories().subscribe(
      categories => this.categories = categories
    );
  }

  delete(category: Category): void {
    swal({
      title: 'Are you sure?',
      text: `Are you sure you want to delete the category ${category.name} ?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.categoryService.delete(category.idcategory).subscribe(
          response => {
            this.categories = this.categories.filter(cli => cli !== category)
            swal(
              'Category deleted!',
              `Category ${category.name} deleted succesfully.`,
              'success'
            )
          }
        )

      }
    })
  }

}
