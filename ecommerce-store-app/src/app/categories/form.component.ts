import { Component, OnInit } from '@angular/core';
import {Category} from './category'
import {CategoryService} from './category.service'
import {Router, ActivatedRoute} from '@angular/router'
import swal from 'sweetalert2'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private category: Category = new Category()
  private titulo:string = "Create Category"

  constructor(private categoryService: CategoryService,
  private router: Router,
  private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  
    this.loadCategory()
  }

  loadCategory(): void{
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if(id){
        this.titulo="Edit category";
        this.categoryService.getCategory(id).subscribe( (category) => this.category = category)
      }
    })
  }

  create(): void {
    this.categoryService.create(this.category)
      .subscribe(category => {
        this.router.navigate(['/categories'])
        swal('Category', `Category ${category.name} created succesfully!`, 'success')
      }
      );
  }

  update():void{
    this.categoryService.update(this.category)
    .subscribe( category => {
      this.router.navigate(['/categories'])
      swal('Category ', `Category ${category.name} updated succesfully!`, 'success')
    }

    )
  }

}
