import { Injectable } from '@angular/core';
import { Category } from './category';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class CategoryService {
  private urlEndPoint: string = 'http://localhost:9090/api/store/sc/categories';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http: HttpClient) { }

  getCategories(): Observable<Category[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map(response => response as Category[])
    );
  }

  create(category: Category) : Observable<Category> {
    category.idcategory =null;
    return this.http.post<Category>(this.urlEndPoint, category, {headers: this.httpHeaders})
  }

  getCategory(idcategory): Observable<Category>{
    return this.http.get<Category>(`${this.urlEndPoint}/${idcategory}`)
  }

  update(category: Category): Observable<Category>{
    return this.http.put<Category>(`${this.urlEndPoint}`, category, {headers: this.httpHeaders})
  }

  delete(idcategory: number): Observable<Category>{
    return this.http.delete<Category>(`${this.urlEndPoint}/${idcategory}`, {headers: this.httpHeaders})
  }

}
