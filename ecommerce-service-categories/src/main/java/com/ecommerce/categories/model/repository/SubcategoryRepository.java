/**
 * 
 */
package com.ecommerce.categories.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.common.models.entity.Subcategory;

/**
 * <Descripcion de la clase>
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
@Repository
public interface SubcategoryRepository extends JpaRepository<Subcategory, Integer>  {

}
