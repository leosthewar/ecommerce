/**
 * 
 */
package com.ecommerce.categories.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.categories.service.CategoryService;
import com.ecommerce.common.models.entity.Category;

/**
 * <Descripcion de la clase>
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/")
public class CategoryController {
	
	private  final Logger log = LoggerFactory.getLogger(CategoryController.class);

	@Autowired
	private CategoryService categoryService;

	
	@GetMapping("/categories")
	public List<Category> findAll() {
		log.info("REST request to get All  Categories");
		return categoryService.findAll();
	}

	@ResponseStatus(HttpStatus.OK)
	@PostMapping("/categories")
	public Category save(@RequestBody Category category) {
		log.info("REST request to save  category {}",category);
		return categoryService.save(category);
	}
	
	
	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/categories")
	public Category update(@RequestBody Category category) {
		log.info("REST request to update  category {}",category);
		return categoryService.save(category);
	}

	@GetMapping("/categories/{id}")
	public ResponseEntity<Category> findById(@PathVariable Integer id) {
		log.info("REST request to find  category {}",id);
		Optional<Category> category =categoryService.findById(id);
		return category.map(response -> ResponseEntity.ok().body(response))
	            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
		
	}
	
	
	@DeleteMapping("/categories/{id}")
	public void delete(@PathVariable Integer id) {
		log.info("REST request to delete  category {}",id);
		categoryService.delete(id);
	}
	
}
