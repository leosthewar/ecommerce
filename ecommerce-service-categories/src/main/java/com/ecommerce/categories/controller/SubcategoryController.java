/**
 * 
 */
package com.ecommerce.categories.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.categories.service.SubcategoryService;
import com.ecommerce.common.models.entity.Subcategory;

/**
 * <Descripcion de la clase>
 * 
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
@RestController
@RequestMapping("/")
public class SubcategoryController {

	private final Logger log = LoggerFactory.getLogger(SubcategoryController.class);

	@Autowired
	private SubcategoryService subcategoryService;

	@GetMapping("/subcategories")
	public List<Subcategory> findAll() {
		log.info("REST request to get All  Categories");
		return subcategoryService.findAll();
	}

	@ResponseStatus(HttpStatus.OK)
	@PostMapping("/subcategories")
	public Subcategory save(@RequestBody Subcategory subcategory) {
		return subcategoryService.save(subcategory);
	}
	
	@ResponseStatus(HttpStatus.OK)
	@PutMapping("/subcategories")
	public Subcategory update(@RequestBody Subcategory subcategory) {
		return subcategoryService.save(subcategory);
	}

	@GetMapping("/subcategories/{id}")
	public ResponseEntity<Subcategory> findById(@PathVariable Integer id) {
		Optional<Subcategory> subcategory = subcategoryService.findById(id);
		return subcategory.map(response -> ResponseEntity.ok().body(response))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}

	@DeleteMapping("/subcategories/{id}")
	public void delete(@PathVariable Integer id) {
		subcategoryService.delete(id);
	}

}
