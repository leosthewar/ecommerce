/**
 * 
 */
package com.ecommerce.categories.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.categories.model.repository.CategoryRepository;
import com.ecommerce.categories.service.CategoryService;
import com.ecommerce.common.models.entity.Category;

/**
 * <Descripcion de la clase>
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
@Service
public class CategoryServiceImpl  implements CategoryService{
	

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Transactional(readOnly = true)
	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	@Override
	@Transactional
	public Category save(Category category) {
		return categoryRepository.save(category);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Category> findById(Integer id) {
		return categoryRepository.findById(id);
	}
	
	@Override
	@Transactional
	public void delete(Integer id) {
		categoryRepository.deleteById(id);
	}
	
}
