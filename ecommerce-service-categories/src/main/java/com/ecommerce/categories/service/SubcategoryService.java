/**
 * 
 */
package com.ecommerce.categories.service;

import java.util.List;
import java.util.Optional;

import com.ecommerce.common.models.entity.Subcategory;

/**
 * <Descripcion de la clase>
 * 
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
public interface SubcategoryService {

	public List<Subcategory> findAll();

	public Subcategory save(Subcategory subcategory);

	public Optional<Subcategory> findById(Integer id);

	public void delete(Integer id);

}
