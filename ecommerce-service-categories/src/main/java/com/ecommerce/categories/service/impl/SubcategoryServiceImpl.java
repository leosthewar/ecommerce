/**
 * 
 */
package com.ecommerce.categories.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.categories.model.repository.SubcategoryRepository;
import com.ecommerce.categories.service.SubcategoryService;
import com.ecommerce.common.models.entity.Subcategory;

/**
 * <Descripcion de la clase>
 * 
 * @autor Leonardo Sthewar Rincon <br/>
 *        Email: leo.sthewar.rincon@gmail.com <br/>
 * @date 09-10-2019
 * @version 1.0
 */
@Service
public class SubcategoryServiceImpl implements SubcategoryService {

	@Autowired
	private SubcategoryRepository subcategoryRepository;

	@Override
	@Transactional(readOnly = true)
	public List<Subcategory> findAll() {
		return subcategoryRepository.findAll();
	}

	@Override
	@Transactional
	public Subcategory save(Subcategory subcategory) {
		return subcategoryRepository.save(subcategory);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Subcategory> findById(Integer id) {
		return subcategoryRepository.findById(id);
	}

	@Override
	@Transactional
	public void delete(Integer id) {
		subcategoryRepository.deleteById(id);

	}

}
